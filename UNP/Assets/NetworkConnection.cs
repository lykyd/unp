﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkConnection : MonoBehaviourPunCallbacks
{
    private void Start() {
        print("Connecting to server...");
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster() {
        print("Connected to server...");
    }

    public override void OnDisconnected(Photon.Realtime.DisconnectCause cause)
    {
        print("Disconected from server - REASON: " + cause.ToString());
    }
}
