﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneTransitions
{
    FADEIN,
    SQUARE,
    CUSTOM,
}

public class SceneChanger : MonoBehaviour
{

    public SceneTransitions scene = SceneTransitions.FADEIN;
    [SerializeField] float timer = 1f;
    [SerializeField] GameObject[] transitions;
    GameObject activeTransition;
    private void Start()
    {
        SelectScreenChanger();
    }

    private void SelectScreenChanger()
    {
        switch (scene)
        {
            case SceneTransitions.FADEIN: DisableAllTransitions(); activeTransition = transitions[0]; activeTransition.SetActive(true); break;
            case SceneTransitions.SQUARE: DisableAllTransitions(); activeTransition = transitions[1]; activeTransition.SetActive(true); break;
            case SceneTransitions.CUSTOM: DisableAllTransitions(); activeTransition = transitions[2]; activeTransition.SetActive(true); break;
            default: DisableAllTransitions(); transitions[0].SetActive(true); break;
        }
    }
    private void DisableAllTransitions()
    {
        foreach (var transition in transitions)
        {
            transition.SetActive(false);
        }
    }

    public void LoadSceneFromOutside(int buildIndex)
    {
        StartCoroutine(LoadSceneWithTimer(buildIndex));
    }

    private IEnumerator LoadSceneWithTimer(int buildIndex)
    {
        //Play Animation
        activeTransition.GetComponent<Animator>().SetTrigger("SceneChange");
        yield return new WaitForSeconds(timer);
        if (SceneManager.sceneCountInBuildSettings == buildIndex) buildIndex = 0;
        SceneManager.LoadScene(buildIndex);
    }
}
