﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
public class TriggerSceneChange : MonoBehaviour
{
    public InputAction fireAction;

     void Awake()
    {
        fireAction.performed += Fire;
    }

    void OnEnable()
    {
        fireAction.Enable();
    }

    void OnDisable()
    {
        fireAction.Disable();
    }

    void Fire(InputAction.CallbackContext ctx)
    {
        FindObjectOfType<SceneChanger>().LoadSceneFromOutside(SceneManager.GetActiveScene().buildIndex + 1);

    }
}
