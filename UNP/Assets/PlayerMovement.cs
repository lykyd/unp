﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerMovement : MonoBehaviour
{
    private MasterInput controls;
    public float speed = 10f;
    Vector2 movementDirection;

    void Awake()
    {
        controls = new MasterInput();
    }

    void Start()
    {
    }
    private void Move(){
        Vector2 currentPos = transform.position;
        Vector2 desiredPositionDirection = controls.Player.Move.ReadValue<Vector2>();

        currentPos.x += desiredPositionDirection.x * speed * Time.deltaTime;
        currentPos.y += desiredPositionDirection.y * speed * Time.deltaTime;
        transform.position = currentPos;
    }

    private void Update() {
        Move();
    }
    void OnEnable()
    {
        controls.Enable();
    }

    void OnDisable()
    {
        controls.Disable();
    }

}
